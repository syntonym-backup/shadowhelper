NOT WORKING ATM -- ALPHA

Pen and Paper in a lightweight system.

Shadowhelper lets you play pen and paper online with your friends.
Its main focus is on a lightweight story driven system
- any system you know the rules of is possible.

* Support for character creation and bookkeeping
* Locations
* Skill checks show information only players that should have them
* Supports asynchronous play

Currently this is only an alpha and not all features are implemented.
If you are still interested in trying it, email me at me [at] syntonym <dot> de .

##webstack

* Flask for glue
* flask-login for userlogin
* sqlalchemy for ORM
* postgresql for database

