import flask
from flask import Flask
from flask import render_template, request, url_for, redirect

from flask.ext.sqlalchemy import SQLAlchemy
from flask.ext.login import LoginManager

from flask.ext.login import login_user, logout_user, current_user, login_required

from functools import wraps, partial
from collections import namedtuple


app = Flask(__name__)
app.config.from_object("config")

db = SQLAlchemy(app)
login_manager = LoginManager(app)

if not app.debug:
    import logging
    from logging import FileHandler
    file_handler = FileHandler(app.config["LOG_FILE"])
    file_handler.setLevel(app.config["LOG_LEVEL"])
    app.logger.addHandler(file_handler)

menu = []
MenuItem = namedtuple("MenuItem", ["name", "url", "login"])
render_template = partial(render_template, menu=menu)

def in_menu(login=False):
    def register_in_menu(view):
        menu.append(MenuItem(view.__name__, view.__name__, login))
        return(view)
    return(register_in_menu)

@login_manager.user_loader
def load_user(id):
    return User.query.get(int(id))

class Post(db.Model):
    id = db.Column(db.Integer, primary_key=True)

class User(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    password = db.Column(db.String(30))
    name = db.Column(db.String(80))
    email = db.Column(db.String(120))

    def __init__(self, password, name, email):
        self.password = password
        self.name = name
        self.email = email

    def is_authenticated(self):
        return True

    def is_active(self):
        return True

    def is_anonymous(self):
        return False

    def get_id(self):
        return str(self.id)

    def __repr__(self):
        return '<User %r>' % (self.name)

@in_menu(login=False)
@app.route("/register")
def register():
    return(render_template("register.html"))

@app.route("/")
def index():
    if current_user.is_authenticated():
        return(render_template("dashboard.html"))
    else:
        return(render_template("index.html"))

@in_menu(login=False)
@app.route('/login', methods=['GET', 'POST'])
def login():
    if request.method == "POST":
        user = User.query.filter_by(name=request.form["username"]).first()
        if user is None:
            flask.abort(404)
        if user.password == request.form["password"]:
            login_user(user)

        return flask.redirect(url_for('index'))
    elif request.method == "GET":
        return(render_template("login.html"))
    else:
        flask.abort(404)

@in_menu(login=True)
@app.route("/logout")
@login_required
def logout():
    logout_user()
    return(redirect(url_for("index")))

if __name__ == "__main__":
    app.run(debug=True)
