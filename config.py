import logging
from os.path import expanduser

SQLALCHEMY_DATABASE_URI = "postgresql+psycopg2://shadow:shadowrun@localhost/shadow"
SECRET_KEY = "SET THIS TO ANY SECRET KEY LIKE A RANDOM HASH!"
LOG_FILE = expanduser("~/git/shadow/shadowhelper.log")
LOG_LEVEL = logging.DEBUG
